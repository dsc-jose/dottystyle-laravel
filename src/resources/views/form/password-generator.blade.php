<div class="password-generator {{ $class ?? '' }}" id="{{ $id }}-generator">
    @include('dottystyle::form.password', compact('id', 'password_name', 'placeholder', 'value'))
    <button type="button" class="btn">{{ $buttonLabel ?? 'Generate Password' }}</button>
</div>
<script>
@component('dottystyle::includes.with-app', ['check' => false])
    $(function() {
        var $generator = $('#{{ $id }}-generator');
        $password = $generator.find(':input.password');

        $generator.on('click', '.btn', function(evt) {
            evt.preventDefault();
            var password = app().utils.generatePassword({ length: 12 });
            $password.val(password);
            $generator.trigger('password:generated', [ password, $password ])
        });
    });
@endcomponent
</script>