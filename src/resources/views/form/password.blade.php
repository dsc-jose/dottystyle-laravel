@php
    $maskedClass = $maskedClass ?? 'fas fa-eye';
    $unmaskedClass = $unmaskedClass ?? 'fas fa-eye-slash';
@endphp
<input 
    id="{{ $id }}"
    class="form-control password" 
    name="{{ $password_name ?? 'password' }}" 
    type="password" 
    placeholder="{{ $placeholder ?? 'Password' }}" 
    value="{{ $value ?? '' }}" />
<i class="toggle {{ $maskedClass }}" id="{{ $id }}-toggle-masked"></i>
<script>
(function($) {

var selector = '#{{ $id }}';
var $password = $(selector);
var $toggle = $(selector + '-toggle-masked');
var masked = true;

$toggle.on('click', function() {
  $password.attr('type', masked ? 'text' : 'password');

  if (masked) {
    $toggle.removeClass('{{ $maskedClass }}').addClass('{{ $unmaskedClass }}');
  } else {
    $toggle.removeClass('{{ $unmaskedClass }}').addClass('{{ $maskedClass }}');
  }

  masked = !masked;
  
  return false;
});

})(jQuery);
</script>