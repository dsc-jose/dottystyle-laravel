(function($) {

var appName = "{{ $appVariable }}";

@if ($check ?? true) 
    if (typeof window[appName] === 'undefined' || !window[appName]) { return; }
@endif

function app() { return window[appName] };

{!! $slot !!}

})(jQuery);