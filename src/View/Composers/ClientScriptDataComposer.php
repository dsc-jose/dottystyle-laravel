<?php

namespace Dottystyle\Laravel\View\Composers;

use Dottystyle\Laravel\Contracts\ClientScriptDataProvider;
use Illuminate\View\View;

class ClientScriptDataComposer 
{
    /**
     * @var string
     */
    protected $varName;

    /**
     * @var \Dottystyle\Laravel\View\ClientScriptDataProvider
     */
    protected $dataProvider;

    /**
     * @param \Dottystyle\Laravel\View\ClientScriptDataProvider $dataProvider
     * @param string $varName
     */
    public function __construct(ClientScriptDataProvider $dataProvider, string $varName)
    {
        $this->varName = $varName;
        $this->dataProvider = $dataProvider;
    }

    /**
     * Bind application data to the view. 
     * 
     * @param \Illuminate\View\View $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with($this->varName, $this->dataProvider->getData());
    }
}