<?php

namespace Dottystyle\Laravel\View;

use Illuminate\Support\Facades\Blade;
use Dottystyle\Laravel\Contracts\ClientScriptDataProvider;
use Dottystyle\Laravel\View\Composers\ClientScriptDataComposer;
use Dottystyle\Laravel\Support\ServiceProvider;
use Illuminate\View\View;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * All of the container singletons that should be registered.
     *
     * @var array
     */
    public $singletons = [
        ClientScriptDataProvider::class => BasicClientScriptDataProvider::class
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__.'/config.php' => config_path($this->ns('view.php'))
        ]);

        $this->mergeConfigFrom(__DIR__.'/config.php', $this->getConfigKey('view'));
        // Register our views
        $this->loadViewsFrom(dirname(__DIR__).'/resources/views', $this->ns());

        $this->registerClientScriptDataComposer();

        //$this->addWithAppDirective();
    }

    /**
     * 
     * @return void
     */
    public function register()
    {
    }
    
    /**
     * Register the view composer for client data script.
     * 
     * @return void
     */
    protected function registerClientScriptDataComposer()
    {
        $config = $this->app['config']->get($this->getConfigKey('view.client_script_data'));
        $viewName = $config['view'];
        $varName = $config['variable_name'];
        $appVariable = $this->app['config']->get($this->getConfigKey('view.client_script_app_variable'));

        $this->app['view']->composer($viewName, function (View $view) use ($varName) {
            $composer = new ClientScriptDataComposer($this->app->make(ClientScriptDataProvider::class), $varName);
            $composer->compose($view);
        });

        // Provide the client application script variable name
        $this->app['view']->composer('dottystyle::includes.with-app', function(View $view) use ($appVariable) {
            $view->with('appVariable', $appVariable);
        });
    }

    /**
     * 
     * @return void
     */
    protected function addWithAppDirective()
    {
        Blade::directive('withApp', function () {
            print_r(func_get_args());
            exit;
            return view('dottystyle::includes.with-app');
        });
    }
}