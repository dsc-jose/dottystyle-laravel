import randomString from 'randomstring'

const generateRandomString = (options={}) => randomString.generate(options)

const addPasswordGeneratorService = (defaultOptions={}) => (app) => {
  const factory = (container) => (options) => generateRandomString({ ...defaultOptions, ...options })
  app.factory('passwordGenerator', factory)

  return app
}

export default addPasswordGeneratorService