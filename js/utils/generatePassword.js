import randomString from 'randomstring'

export default function generateRandomString(options={}) {
  return randomString.generate(options)
}