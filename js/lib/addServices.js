/**
 * 
 * @param {App} app
 * @param {Function} ...serviceWrappers
 * @return {App}
 */
export default function addServices(app, ...serviceWrappers) {
  return serviceWrappers.reduce((newApp, wrapper) => wrapper(newApp), app)
}