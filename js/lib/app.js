import $ from 'jquery'
import eventEmitter from 'event-emitter'
import _each from 'lodash/forEach'
import _pick from 'lodash/pick'

class App {
  constructor({ loadPage, loadModule, methods }) {
    this.modules = {}
    this.loadPromise = new Promise((resolve) => this.triggerLoad = resolve)
    this._loadPage = loadPage
    this._loadModule = loadModule
    this.events = eventEmitter()

    $(this.handleDocumentLoad)

    // Bind additional methods to the application object
    if (typeof methods === 'object') {
      _each(methods, (fn, method) => this[method] = fn.bind(this))
    }
  }

  /**
   * Trigger the load event of the application, effectively calling all load callback attached. 
   * 
   * @return {void}
   */
  handleDocumentLoad = () => {
    this.triggerLoad(this)
  }

  /**
   * Get a module on the application
   * 
   * @param {String} name
   * @param {Object} [options]
   * @return {Promise}
   */
  async getModule(name) {
    if (!this.isModuleLoaded(name)) {
      try {
        // Load the module once
        const module = await this._loadModule(name, this)
        
        if (!module) {
          throw "Invalid module returned"
        }

        this.modules[name] = module
      } catch (e) {
        this.events.emit('moduleLoadError', e)
        throw e
      }
    }

    return this.modules[name]
  }

  /**
   * Determine whether module has been loaded or not.
   * 
   * @param {String} name 
   * @return {Boolean}
   */
  isModuleLoaded(name) {
    return typeof this.modules[name] !== 'undefined'
  }

  /**
   * Load a page
   * 
   * @param {String} name
   * @param {*} [options]
   * @return {Promise}
   */
  async loadPage(name, options=undefined) {
    try {
      const page = await this._loadPage(name, this, options)

      if (! page) {
        throw "Invalid page"
      }

      return page
    } catch (e) {
      this.events.emit('pageLoadError', name)
      throw e
    }
  }

  /**
   * Add an callback when application has been loaded.
   * 
   * @param {Function} callback
   * @return {void}
   */
  on(event, callback) {
    if (event === 'load') {
      this.loadPromise.then(callback)
    } else {
      this.events.on(event, callback)
    }
  }

  /**
   * Remove listener for an event.
   * 
   * @param {String} event
   * @param {Function} callback
   * @return {void}
   */
  off(event, callback) {
    this.events.off(event, callback) 
  }
}

const createApp = (options) => {
  return new App(_pick(options, [
    'loadPage',
    'loadModule',
    'methods'
  ]))
}

export default createApp