let mix = require('laravel-mix')
let argv = require('yargs').argv
let CleanWebpackPlugin = require('clean-webpack-plugin') 

const shouldAddVersion = (options) => mix.inProduction() && (options.versioned || (typeof argv.env === 'object' && argv.env.versioned))

class SetupExtension {
  name() {
    return 'setup'
  }

  register(options={}) {
    if (typeof options.cleanOnProd === 'object') {
      this.cleanOnProd = {
        paths: [], 
        options: {},
        ...options.cleanOnProd
      }
    }

    if (shouldAddVersion(options)) {
      mix.version()
    }
  }

  webpackRules() {
    return {
      test: /\.jsx?$/,
      exclude: /(node_modules(?!\/dottystyle-laravel)|bower_components)/,
      use: [
        {
          loader: 'babel-loader',
          options: Config.babel()
        }
      ]
    }
  }

  webpackPlugins() {
    const plugins = []

    // Clean files when building for production
    if (this.cleanOnProd) {
      const { paths, ...cleanOptions } = this.cleanOnProd
      plugins.push(new CleanWebpackPlugin(paths, cleanOptions))
    }

    return plugins
  }

  webpackConfig(config) {
    config.externals || (config.externals = {})
    config.externals.jquery || (config.externals.jquery = 'jQuery')
  }
}

mix.extend('setup', new SetupExtension())