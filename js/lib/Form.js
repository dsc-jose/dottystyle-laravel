import _reduce from 'lodash/reduce' 
import 'jquery-serializejson'

export class SubmissionError {
  constructor(errors) {
    const { _error, ...fieldErrors } = errors
    // Remove general error
    this._generalError = _error
    this.errors = fieldErrors
  }

  hasGeneralError() {
    return typeof this._generalError !== 'undefined'
  }

  getGeneralError() {
    return this._generalError
  }
}

const defaultSerializeValues = (form) => form.serializeJSON()

class Form {
  constructor(params, toast) {

    $.validator.addMethod("email", function(value, element) {
      return this.optional( element ) || /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test( value );
    }, 'Please enter a valid email address.');

    const { form, rules, submit, onSubmitSuccess, onSubmitFail, serializeValues, reset } = params
    this.submitHandler = submit
    form.submit(event => event.preventDefault())
      .validate({
        rules,
        onfocusout: function(element) {
          this.element(element)
        },
        submitHandler: this.submit
      })
    this.form = form
    this.onSubmitSuccess = onSubmitSuccess
    this.onSubmitFail = onSubmitFail
    this.serializeValues = serializeValues || defaultSerializeValues
    this.toast = toast
    this.reset = reset || false
  }

  submit = async () => {
    try {
      this.form.find("input[type=submit]").prop('disabled', true)
      const values = this.serializeValues(this.form, this)
      const result = await this.submitHandler(values, this)
      this.onSubmitSuccess(result, values, this)
      
      if(this.reset) 
        this.form[0].reset()

    } catch (err) {
      if (err instanceof SubmissionError) {
        this.showSubmissionErrors(err)

        // show error messages here
        //err.messages 
        this.handleSubmitFail(err)
      } else {
        this.handleSubmitFail(err)
      }
    } finally {
      this.form.find("input[type=submit] ").prop("disabled", false)
    }
  }
    
  /**
   * 
   * @param {*} errors 
   */
  showSubmissionErrors(submissionError) {
    // Filter first error message for each key
    const _errors = _reduce(submissionError.errors, (result, messages, key) => {
      return Object.assign(result, { [key]: messages[0] })
    }, {})
    
    this.form.validate().showErrors(_errors)

    // Show general error 
    submissionError.hasGeneralError() && this.toast.error(submissionError.getGeneralError(), 'Error')
  }

  handleSubmitFail = (err) => {
    if (typeof this.onSubmitFail  === 'function') {
      this.onSubmitFail(err)
    } else {
      throw err
    }
    this.form.find("input[type=submit] ").removeAttr("disabled")
  }
}

export const handleApiResponse = (callback ) => {
  return async (values, form) => {
    const response = await callback(values, form)
    
    if (response.ok) {
      return response
    } else if (response.data.errors && response.status === 422) {
      // show errors on form
      throw new SubmissionError(response.data.errors)
    } else {
      throw new SubmissionError({ _error: 'Ooops! Something went wrong' })
    }
  }
}

export default Form
