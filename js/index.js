import createApp from './lib/app'
import addServices from './lib/addServices'
import Page from './lib/Page'
import Form from './lib/Form'

export default createApp

export { 
  Form,
  Page,
  addServices
}